package dao;

import java.sql.Connection;
import java.util.List;

// classe générique qui contient méthodes CRUD
// T == n'importe qu'elle classe
public abstract class DAO<T> {

	// connexion BDD
	public Connection connect = ConnexionPostgreSQL.getInstance();

	// méthode CRUD
	public abstract void create(T obj);
	
	public abstract T read(String code);
	
	public abstract void update(T obj);
	
	public abstract void delete(String code);

	// lis tous enregistrement table et renvoie liste
	public abstract List<T> recupAll();
}
