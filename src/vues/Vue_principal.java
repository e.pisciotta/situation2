package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;  
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;

import java.sql.SQLException;
import java.util.List;
import java.awt.event.ActionEvent;

import classes.*;
import dao.*;
import controleurs.*;

import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;

// 
public class Vue_principal extends JFrame {
	// création objet controleur instance classe Controleur_principal
	// controleur est connu de la vue
	protected Controleur_principal controleur;


	private JPanel contentPane;
	private JTextField txtChoisirUnClub;
	private JTextField txtChoisirUnClub_1;
	
	public JButton btnQuitter;
	private JFormattedTextField text_date_création;
	private JTextField text_description;
	private JTextField text_image;
	private JTextField text_prix;
	private JTextField text_cpu;
	private JTextField text_se;
	private JTextField text_ram;
	private JTextField text_cg;
	private JTextField text_hdd;
	private JTextField text_nom;
	private JLabel lblDescritpion;
	private JLabel lblDateDeCration;
	private JLabel lblNewLabel;
	private JLabel lblPrix;
	private JLabel lblCpu;
	private JLabel lblSystmeDexploitation;
	private JLabel lblRam;
	private JLabel lblCarteGraphique;
	private JLabel lblHdd;
	private JLabel lblNom;
	private JLabel lblNewLabel_1;
	
	/**
	 * Launch the application.
	 */
	/*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vue_principal frame = new Vue_principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */

	// constructeur de la vue
	public Vue_principal(Controleur_principal unControleur) {
		this.controleur=unControleur;
		// définition vue
		setTitle("Les Jeux");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 654, 584);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtChoisirUnClub_1 = new JTextField();
		txtChoisirUnClub_1.setEditable(false);
		txtChoisirUnClub_1.setText("Insertion d'un nouveau jeu :");
		txtChoisirUnClub_1.setBounds(29, 12, 210, 19);
		contentPane.add(txtChoisirUnClub_1);
		txtChoisirUnClub_1.setColumns(10);
		
		// création bouton
		JButton btnAjouter = new JButton("Ajouter");
		// nomme action lié au bouton
		btnAjouter.setActionCommand("submit");
		// ajout action gérer par le controleur
		btnAjouter.addActionListener(controleur);
		btnAjouter.setBounds(46, 458, 117, 25);
		contentPane.add(btnAjouter);
		
		
		
		btnQuitter = new JButton("Quitter");
		btnQuitter.addActionListener(controleur);
		btnQuitter.setActionCommand("quit");
		btnQuitter.setBounds(447, 458, 117, 25);
		contentPane.add(btnQuitter);
		
		 //DateFormat dateFormat = new SimpleDateFormat("dd MM YYYY");
	    //  JFormattedTextField today = new JFormattedTextField(dateFormat);
	      
		text_date_création = new JFormattedTextField();
		 
		text_date_création.setBounds(195, 95, 150, 19);
		contentPane.add(text_date_création);
		text_date_création.setColumns(10);
		
		text_description = new JTextField();
		text_description.setBounds(195, 63, 150, 19);
		contentPane.add(text_description);
		text_description.setColumns(10);
		
		text_image = new JTextField();
		text_image.setBounds(195, 126, 150, 19);
		contentPane.add(text_image);
		text_image.setColumns(10);
		
		text_prix = new JTextField();
		text_prix.setBounds(195, 157, 150, 19);
		contentPane.add(text_prix);
		text_prix.setColumns(10);
		
		text_cpu = new JTextField();
		text_cpu.setBounds(195, 188, 150, 19);
		contentPane.add(text_cpu);
		text_cpu.setColumns(10);
		
		text_se = new JTextField();
		text_se.setBounds(231, 219, 114, 19);
		contentPane.add(text_se);
		text_se.setColumns(10);
		
		text_ram = new JTextField();
		text_ram.setBounds(195, 250, 150, 19);
		contentPane.add(text_ram);
		text_ram.setColumns(10);
		
		text_cg = new JTextField();
		text_cg.setBounds(195, 281, 150, 19);
		contentPane.add(text_cg);
		text_cg.setColumns(10);
		
		text_hdd = new JTextField();
		text_hdd.setBounds(195, 312, 150, 19);
		contentPane.add(text_hdd);
		text_hdd.setColumns(10);
		
		text_nom = new JTextField();
		text_nom.setBounds(195, 343, 150, 19);
		contentPane.add(text_nom);
		text_nom.setColumns(10);
		
		lblDescritpion = new JLabel("Description :");
		lblDescritpion.setBounds(46, 65, 137, 15);
		contentPane.add(lblDescritpion);
		
		lblDateDeCration = new JLabel("Date de création :");
		lblDateDeCration.setBounds(46, 95, 140, 15);
		contentPane.add(lblDateDeCration);
		
		lblNewLabel = new JLabel("Image :");
		lblNewLabel.setBounds(46, 128, 130, 15);
		contentPane.add(lblNewLabel);
		
		lblPrix = new JLabel("Prix :");
		lblPrix.setBounds(46, 159, 70, 15);
		contentPane.add(lblPrix);
		
		lblCpu = new JLabel("CPU :");
		lblCpu.setBounds(46, 190, 70, 15);
		contentPane.add(lblCpu);
		
		lblSystmeDexploitation = new JLabel("Système d'exploitation :");
		lblSystmeDexploitation.setBounds(46, 219, 193, 15);
		contentPane.add(lblSystmeDexploitation);
		
		lblRam = new JLabel("RAM :");
		lblRam.setBounds(46, 252, 150, 15);
		contentPane.add(lblRam);
		
		lblCarteGraphique = new JLabel("Carte Graphique :");
		lblCarteGraphique.setBounds(46, 283, 150, 15);
		contentPane.add(lblCarteGraphique);
		
		lblHdd = new JLabel("HDD :");
		lblHdd.setBounds(46, 314, 70, 15);
		contentPane.add(lblHdd);
		
		lblNom = new JLabel("Nom :");
		lblNom.setBounds(46, 345, 70, 15);
		contentPane.add(lblNom);
		
		lblNewLabel_1 = new JLabel("ex : JJ-MM-YYYY");
		lblNewLabel_1.setBounds(367, 74, 166, 60);
		contentPane.add(lblNewLabel_1);
		
		
	}
	
	
	
	
	
	public Jeu action_enregistrer() {
		if(text_date_création.getText() != null && text_description.getText() != null && text_image.getText() != null && text_prix.getText() != null && text_cpu.getText() != null && text_se.getText() != null && text_ram.getText() != null && text_cg.getText() != null && text_hdd.getText() != null && text_nom.getText() != null) {
			
			String code_1 = text_date_création.getText();
			String code_2 = text_description.getText();
			String code_3 = text_image.getText();
			String code_4 = text_prix.getText();
			String code_5 = text_cpu.getText();
			String code_6 = text_se.getText();
			String code_7 = text_ram.getText();
			String code_8 = text_cg.getText();
			String code_9 = text_hdd.getText();
			String code_10= text_nom.getText();
			
			Double a = Double.parseDouble(code_4);
			

			  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			  

			  //convert String to LocalDate
			  LocalDate d = LocalDate.parse(code_1, formatter);
 
			 
		//		d = (java.sql.Date) new Date(text_date_création.getText());
			 
			Jeu c = new Jeu(code_2,d,code_3,a,code_5,code_6,code_7,code_8,code_9,code_10);
			
			return c;
		}
		
		else {
			
			return null;
		}
		
	}
}

