package classes;

 
import java.time.LocalDate;

/**
 * Définition de la Classe Jeu
 * 
 * @author Valentine
 *
 */

public class Jeu {

	// Definitions des proprietes privees
	
		private int id_Jeu;
		private String description;
		private LocalDate date_creation;
		private String image;
		private Double prix;
		private String cpu;
		private String se;
		private String ram;
		private String carte_graphique;
		private String hdd;
		private String nom;
		
		
		// Definitions des accesseurs et mutateur
		
		/**
		 *
		 * Permet d'obtenir l'id d'un Jeu
		 * @return - l'id du Jeu sous forme d'entier
		 */
		public int getId_Jeu() { 
			return id_Jeu;
		}
		
		/**
		 *
		 * Permet la modification de l'id d'un Jeu
		 * @return - le nouvel id � affecter
		 */

		public void setId_Jeu(int id_Jeu) {
			this.id_Jeu = id_Jeu;
		}
		
		/**
		 * Permet d'obtenir la description d'un Jeu
		 * @return - la description d'un Jeu sous forme de chaine de caract�re
		 */
		public String getDescription() {
			return description;
		}
		
		/**
		 * Permet la modification de la description d'un Jeu
		 * @return - la nouvelle description � affecter
		 */
		
		public void setDescription(String description) {
			this.description = description;
		}
		
		/**
		 *
		 * Permet d'obtenir la date de cr�ation d'un Jeu
		 * @return - la date de cr�ation du Jeu sous forme de date
		 */
		public LocalDate getDate_creation() { 
			return date_creation;
		}
		
		/**
		 *
		 * Permet la modification de la date de cr�ation du Jeu
		 * @return - la nouvelle date � affecter
		 */

		public void setDate_creation(LocalDate date_creation) {
			this.date_creation = date_creation;
		}
		
		/**
		 *
		 * Permet d'obtenir l'image du Jeu
		 * @return - l'image du Jeu sous forme de chaine de caract�re
		 */
		public String getImage() { 
			return image;
		}
		
		/**
		 *
		 * Permet la modification de l'image du Jeu
		 * @return - la nouvelle image � affecter
		 */

		public void setImage(String image) {
			this.image = image;
		}
		
		/**
		 *
		 * Permet d'obtenir le prix du Jeu
		 * @return - le prix du Jeu sous forme de double
		 */
		public Double getPrix() { 
			return prix;
		}
		
		/**
		 *
		 * Permet la modification du prix du Jeu
		 * @return - le nouveau prix � affecter
		 */

		public void setPrix(Double prix) {
			this.prix = prix;
		}
		
		/**
		 *
		 * Permet d'obtenir le cpu recommand� pour le Jeu
		 * @return - le cpu sous forme de chaine de caract�re
		 */
		public String getCpu() { 
			return cpu;
		}
		
		/**
		 *
		 * Permet la modification du cpu recommand� pour le Jeu
		 * @return - le nouveau cpu � affecter
		 */

		public void setCpu(String cpu) {
			this.cpu = cpu;
		}
		
		/**
		 *
		 * Permet d'obtenir le syst�me d'exploitation recommand� pour le Jeu
		 * @return - le syst�me d'exploitation recommand� pour le Jeu sous forme de chaine de caract�re
		 */
		public String getSe() { 
			return se;
		}
		
		/**
		 *
		 * Permet la modification du syst�me d'exploitation recommand� pour le Jeu
		 * @return - le nouvel syst�me d'exploitation � affecter
		 */

		public void setSe(String se) {
			this.se = se;
		}
		
		/**
		 *
		 * Permet d'obtenir la ram recommand� pour le Jeu
		 * @return - la ram recommand� pour le Jeu sous forme de chaine de caract�re
		 */
		public String getRam() { 
			return ram;
		}
		
		/**
		 *
		 * Permet la modification de la ram recommand� pour le Jeu
		 * @return - la nouvelle ram recommand� pour le Jeu � affecter
		 */

		public void setRam(String ram) {
			this.ram = ram;
		}
		
		/**
		 *
		 * Permet d'obtenir la carte graphique recommand� pour le Jeu
		 * @return - la carte graphique recommand� pour le Jeu sous forme de chaine de caract�re
		 */
		public String getCarte_graphique() { 
			return carte_graphique;
		}
		
		/**
		 *
		 * Permet la modification de la carte graphique recommand� pour le Jeu
		 * @return - la nouvelle carte graphique recommand� pour le Jeu � affecter
		 */

		public void setCarte_graphique(String carte_graphique) {
			this.carte_graphique = carte_graphique;
		}
		
		/**
		 *
		 * Permet d'obtenir le HDD recommand� pour le Jeu
		 * @return - le HDD recommand� pour le Jeu sous forme de chaine de caract�re
		 */
		public String getHdd() { 
			return hdd;
		}
		
		/**
		 *
		 * Permet la modification du HDD recommand� pour le Jeu
		 * @return - le nouveau HDD recommand� pour le Jeu � affecter
		 */

		public void setHdd(String hdd) {
			this.hdd = hdd;
		}
		
		/**
		 *
		 * Permet d'obtenir le nom du Jeu
		 * @return - le nom du Jeu sous forme de chaine de caract�re
		 */
		public String getNom() { 
			return nom;
		}
		
		/**
		 *
		 * Permet la modification du nom du Jeu
		 * @return - le nouveau nom du Jeu � affecter
		 */

		public void setNom(String nom) {
			this.nom = nom;
		}
		
		// Definitions des constructeurs 
		
		/**
		 * Construit un Jeu - Initialise avec des valeurs par d�faut les propri�t�s de la classe
		 */
		
		public Jeu() 
		{
			id_Jeu= 0;
			description=null;
			date_creation=null;
			image=null;
			prix=null;
			cpu=null;
			se=null;
			ram=null;
			carte_graphique=null;
			hdd=null;
			nom=null;
		}
		
		/**
		 * Construit un Jeu avec param�tres alimentant les propri�t�s
		 * @param id_Jeu - alimente la propri�t� id_Jeu
		 * @param description - alimente la propri�t� description
		 * @param date_creation - alimente la propri�t� date_creation
		 * @param image - alimente la propri�t� image
		 * @param prix - alimente la propri�t� prix
		 * @param cpu - alimente la propri�t� cpu
		 * @param se - alimente la propri�t� se
		 * @param ram - alimente la propri�t� ram
		 * @param carte_graphique - alimente la propri�t� carte_graphique
		 * @param hdd - alimente la propri�t� hdd 
		 * @param nom - alimente la propri�t� nom
		 */
		
		public Jeu(int id_Jeu ,String description,LocalDate date_creation,String image,Double prix,String cpu,String se,String ram,String carte_graphique,String hdd,String nom)
		{	
		   this.id_Jeu = id_Jeu;
	       this.description = description;
		   this.date_creation = date_creation;
		   this.image = image;
		   this.prix = prix;
		   this.cpu = cpu;
		   this.se = se;
		   this.ram = ram;
		   this.carte_graphique = carte_graphique;
		   this.hdd = hdd;
		   this.nom = nom;
	    }
	
		public Jeu(String description,LocalDate date_creation,String image,Double prix,String cpu,String se,String ram,String carte_graphique,String hdd,String nom)
		{	
		  
	       this.description = description;
		   this.date_creation = date_creation;
		   this.image = image;
		   this.prix = prix;
		   this.cpu = cpu;
		   this.se = se;
		   this.ram = ram;
		   this.carte_graphique = carte_graphique;
		   this.hdd = hdd;
		   this.nom = nom;
	    }
}
